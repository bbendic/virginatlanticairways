//
//  main.m
//  Virgin Atlantic
//
//  Created by MacBook on 8/4/16.
//  Copyright © 2016 MacBook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
